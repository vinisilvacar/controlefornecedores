package controller;

import java.util.ArrayList;

public class ControleTelefone {
    //Criando Lista de Telefones
    private ArrayList listaTelefones = new ArrayList();
    
    //Adicionando um Telefone ao Fornecedor
    public String AdicionarTelefone(String umTelefone){        
        listaTelefones.add(umTelefone);
        return "telefone adicionado.";        
    }
    //Mostrando Lista de Telefones Adicionados ao Fornecedor
    public ArrayList RetornarLista(){
        return listaTelefones;
    }
    
}
