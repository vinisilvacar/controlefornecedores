package controller;
import model.Endereco; //Importando classe com atributos de endereço

/**
 *
 * @author vinicarvalho
 */
public class ControleEndereco {
    
    private Endereco umEndereco;
    
    //Retornar um endereço o pesquisá-lo
    public Endereco PesquisaEndereco(){
        return umEndereco;
    }
    //Guadar a informação de Endereço - Encapsulamento
    public void ArmazenarEndereco(Endereco umEndereco){
        this.umEndereco = umEndereco;
    }
    
}
