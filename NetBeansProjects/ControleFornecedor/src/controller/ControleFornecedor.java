package controller;
import model.*;
import java.util.ArrayList;

public class ControleFornecedor {
    
    //Criando lista de Forncedores
    private ArrayList<Fornecedor> listaFornecedores;
    
    //Controle
    public ControleFornecedor() {
        listaFornecedores = new ArrayList<Fornecedor>();
}
    //Adicionar Pessoa Física
    public String adicionaFisica(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Fisica adicionado com sucesso";
    }
    
    //Remover Pessoa Física
    public String removerFisica(PessoaFisica fornecedor){
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Fisica removida com sucesso";
    }
    
    //Pesquisar Fornecedor Fisico
    public Fornecedor pesquisarFisico(String umCPF){
        for(Fornecedor umFornecedor : listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umCPF)) return umFornecedor;
	}   return null;
    }
    
    public String pesquisarFisico(int index){
		return listaFornecedores.get(index).getNome();
	}
    
    //Adicionar Pessoa Jurídica
    public String adicionaJuridica(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Juridica adicionado com sucesso";
    }
    
     //Remover Pessoa Jurídica
    public String removerJuridica(PessoaJuridica fornecedor){
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Juridica removida com sucesso";
    }
    
    //Pesquisar Fornecedor
    public Fornecedor pesquisarJuridico(String umCNPJ){
	for(Fornecedor umFornecedor : listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umCNPJ)) return umFornecedor;
	}   return null;
    }
	
	public String pesquisarJuridico(int index){
		return listaFornecedores.get(index).getNome();
	}
}
