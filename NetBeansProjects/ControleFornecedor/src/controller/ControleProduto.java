package controller;
import java.util.ArrayList;

public class ControleProduto {
    
     //Criando lista de Forncedores
    private ArrayList listaProdutos = new ArrayList();
    
    //Adicionar Produto a Lista de Produtos
    public String adicionaProduto(String umProduto){
        listaProdutos.add(umProduto);
        return "Produto adicionado com sucesso";
    }
    
    //Mostrar Lista de Produtos
    public ArrayList mostrarLista(){
        return listaProdutos;
    }
    
    //Remover Produto da Lista
    public String RemoverProduto(String umProduto){
        String mensagem = "Produto removido";
        listaProdutos.remove(umProduto);
        return mensagem;
    }
    
}
