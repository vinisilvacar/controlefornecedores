package model;

public class PessoaJuridica extends Fornecedor {
    //PessoaJuridica herda informações de Fornecedor

    private String cnpj;
    private String razaoSocial;
    
    //Inicia ao ter 3 Strings
    public PessoaJuridica(String nome, String telefone, String cnpj){
        super(nome,telefone); //Herda nome e telefone da classe Fornecedor
        this.cnpj = cnpj;
    }
    
    //Setters e Getters
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    public String getCnpj() {
        return cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }
    
}
