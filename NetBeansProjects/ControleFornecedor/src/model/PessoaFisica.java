package model;

public class PessoaFisica extends Fornecedor { //Aplicando Herança (extends)

    private String cpf;
    
    //Ligação com o construtor que inicia ao ter as 3 Strings
    public PessoaFisica(String nome, String telefone, String cpf){
        super(nome,telefone); //Herda nome e telefone da classe Fornecedor
        this.cpf = cpf;
    }
    
    //Setters e Getters
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }      
}
