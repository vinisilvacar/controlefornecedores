package model;
import java.util.ArrayList;

public class Fornecedor {
    
    protected String nome;
    protected ArrayList<String> telefones; //Criando lista de Telefones
    private Endereco endereco; 
    private ArrayList<Produto> produtos; //Criando lista de Produtos
    
    //Construtor
    public Fornecedor(String nome, String telefone){ //Só irá instanciar se fornecedor tiver um nome e  telefone.
        this.nome = nome;
        telefones.add(telefone);
    }

    //Setters e Getters
    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<String> telefones) {
        this.telefones = telefones;
    }
    
}
