package controleFornecedor;
import model.*;
import java.util.ArrayList;

public class controleFornecedor {
    
    //Criando lista de Forncedores
    private ArrayList<Fornecedor> listaFornecedores;
    
    //Controle
    public controleFornecedor() {
        listaFornecedores = new ArrayList<Fornecedor>();
}
    //Adicionar Pessoa Física
    public String adiciona(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Fisica adicionado com sucesso";
    }
    
    //Adicionar Pessoa Jurídica
    public String adiciona(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Juridica adicionado com sucesso";
    }
}
